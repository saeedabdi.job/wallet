
## backend test wallet

## Installation

```bash
$ npm install
```

## run
```bash
$ npm run dev
```

## DB : mongodb

## docs

### Postman file is located at the root of the project

### get words
```js
request : GET ('http://localhost:3000/api/v1/word')

response :
{
    "success": true,
    "statusCode": 200,
    "message": "random wallet words",
    "data": {
        "words": "grain enough energy bone fox tank glide pluck burger already pelican crawl",
        "hash": "7b3e2ddf020bb7e1ce0a28caceae18f80135939aa784aebf404c83ee15e86e2f2886009385345c0744e7db29a9ea6ff19129c15a655ad6104c6e6890a1d16fe3"
    }
}


```

### register
```js
request : POST ('http://localhost:3000/api/v1/register')

parameter:
{
    username: 'readme'
    password: '123456'
    words_hash: '7b3e2ddf020bb7e1ce0a28caceae18f80135939aa784aebf404c83ee15e86e2f2886009385345c0744e7db29a9ea6ff19129c15a655ad6104c6e6890a1d16fe3'
}

response :

{
    "success": true,
    "statusCode": 200,
    "message": "ok insert word",
    "data": {
        "username": "readme",
        "password": "123456",
        "words_hash": "7b3e2ddf020bb7e1ce0a28caceae18f80135939aa784aebf404c83ee15e86e2f2886009385345c0744e7db29a9ea6ff19129c15a655ad6104c6e6890a1d16fe3",
        "_id": "6215f92dc41949c2ae3572ec",
        "__v": 0
    }
}
```

### recovery password with words
```js
request : POST ('http://localhost:3000/api/v1/forgetPassword-words')

parameter:
{
    username: readme
    words: 'grain enough energy bone fox tank glide pluck burger already pelican crawl'
}

response :

{
    "success": true,
    "statusCode": 200,
    "message": "recovery user successfull",
    "data": "insect badge chapter shoot acid sugar vocal actual select maple enforce flag"
}
```