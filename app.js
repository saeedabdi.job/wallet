const express = require("express");
const app = express();
const cors = require('cors');
require('./src/config/config');
require('./src/lib/basic-methods');


//Setting port
let PORT = 3000;

//initialize cors
app.use(cors());

// body parser
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// routes
app.use('/',require("./src/api-routes/routes-manager"));

// module.exports = app

app.listen(PORT, function () {
  console.log("CORS-enabled web server listening on port " + PORT);
});
