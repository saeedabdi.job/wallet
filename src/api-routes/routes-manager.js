const router = require('express').Router()

router.use('/api/v1',require('./v1-route'));

module.exports = router;