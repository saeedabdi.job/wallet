const v1 = require('express').Router()

const register = require('../controller/v1/public/registerController')
const word = require('../controller/v1/public/wordController')

v1.route('/word').get(word.getWords);
v1.route('/register').post(register);
v1.route('/forgetPassword-words').post(word.forgetPassword)

module.exports = v1;