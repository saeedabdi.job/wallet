// back data to apiCalled

exports.jsonSend = ({res, data, message,success = true}) =>
    res.status(200).json({
        success: success,
        statusCode: 200,
        message: message || 'binance api data',
        data: data,
    })


exports.failRequest = (res, message) => 
    res.status(500).json({
        success: false,
        statusCode: 500,
        message: message || 'error in server',
    })
