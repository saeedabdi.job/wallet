// a singelton class for tools this app
const bip39 = require("bip39");

class Tools {

    static getInstance() {
        if (!Tools.instance)
            Tools.instance = new Tools();

        return Tools.instance;
    }

    sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

    makingPhraseSeed = (words) => {
        let mnemonic;
        if (!words)
            mnemonic = bip39.generateMnemonic()
        else
            mnemonic = words.replace(/\s+/g, ' ').trim()

        console.log(words)
        return {
            words: mnemonic,
            hash: bip39.mnemonicToSeedSync(mnemonic).toString('hex')
        }

    }

}

module.exports = Tools.getInstance();