const mongoose = require("mongoose");

const schema = mongoose.Schema({
    username: String,
    password: String,
    words_hash: String,
})
let User = mongoose.model('users', schema)

module.exports = User